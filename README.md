# README #

The [Name] Microservice description and installation process.

### What is this repository for? ###

* Quick Summary

### Prerequisites ###

The following are prerequisites:

- Azure CLI
- JDK 8
- WSL2 or Linux
- Intellij/Eclipse for java spring boot.

# Setup

### Get source
git clone git@bitbucket.org:krislatham1/blog_microservice.git

### Build

mvn clean
mvn package

# check and stop existing containers
docker container list
docker container stop [cont_id]

# check images and delete existing
docker images
docker image blog-microservice
docker image rm [container_registry_name].azurecr.io/blog-microservice:dev

### Build docker image
docker build -t blog-microservice .

### Test locally
docker container run -p 80:8080 blog-microservice

#### tag the release
docker tag [image_name] [container_registry_name].azurecr.io/[image_name]:[version]

### Deploy to Azure

#### login
az login
az account set -s [acct]

#### username/pass is admin user set in Container Registry -> Settings -> Access Keys
docker login [container_registry_name].azurecr.io

#### push to azure
docker push <acrLoginServer>/[image_name]:[version]

#### login to portal and point app service to new image, restart

